# Snowcrash Base theme

## What it does:
Snowcrash Base is meant to be used as base theme. 
It introduces some regions (see listing below).
The only CSS styling has its purpose only in fixing some backwards compatibility
issues regarding theming overrides and for visualizing the regions.
Theming overrides are introduced for adjusting markup, CSS and variables used in
templates for core as well as for some widely used contrib modules 
like views and panels.

## Regions (in order of markup flow):

Toolbar
Header
Navigation
Highlighted

Content top
Content
Content bottom
Sidebar first
Sidebar second

Footer first
Footer second
Footer third
Footer fourth
Footer
