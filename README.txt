# Snowcrash project

The Snowcrash project contains two themes:
* Snowcrash Base
* Snowcrash

## Snowcrash Base

Obviously Snowcrash Base is meant to be used as base theme, it introduces 
theme overrides for preprocessing, templates and theme functions to enhance
the markup that is output by default by Drupal core and by widely used contrib modules.
Some regions are added with a default stacked layout according to a 
"mobile first" principle. Markup is written in HTML5.
More Infos can be found in the README in the *snowcrash_base* subfolder.

## Snowcrash extends Snowcrash Base

* CSS resets for browsers, drupal styles
* Basic form styling based on formalize (see: http://formalize.me/)
* Grid based layout
* Responsive layout variants for various screen sizes
* CSS compiled from a set of SASS stylesheets
* Images used by CSS combined to sprites

More Infos can be found in the README in the *snowcrash* subfolder.
